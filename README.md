# lexon-remix
Lexon Plugin for Remix IDE.

<screenshot>


## How to get started
### Remote plugin
This plugin is hosted at ...
To use it, open `Settings` tab and click the `Lexon` button in the plugin section.

### Local plugin
You can host this plugin in your local.

```npm install```

```npm start```

Put this JSON in `Settings` tab.
```
{
"title": "Lexon Plugin",
"url": "http://127.0.0.1:3000"
}
```

## How to use plugin
1. Write lexon code(.lex) in the editor
2. Click Compile button
3. Now you can deploy the contract in the Run tab!


### Local Lexon Compiler
You can use your local Lexon compiler by selecting the radio button `Local` .
First, you need to install Lexon.

```install```

(see [installing-lexon](https://)).


### How to push / deploy new lexon-wasm / lexon-remix
* 1. lexons-wasm - basically - bump the version of lexon-wasm in package.json - commit - then tag the latest version in gitlab, it will then deploy to  npm.

* bump the version of lexon-remix - either specify lexon-wasm version in package.json or leave (should defautl to newest 0.2.x) - and then commit and tag the new version in gitlab (or from commandline) then the tagged commit will be deployed and use new lexon-wasm
